use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader, Lines};

pub fn challenge_data(index: &str) -> io::Result<Lines<BufReader<File>>> {
    let input = File::open(format!("inputs/{}.txt", index))?;
    Ok(BufReader::new(input).lines())
}

pub fn start(index: &str) -> io::Result<Lines<BufReader<File>>> {
    env_logger::init();
    println!(
        r#" 
    ###################
    # AdventOfCode {} #
    ###################"#,
        index
    );
    challenge_data(index)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reads_data() {
        let first_data = challenge_data("01").unwrap();
        assert_eq!(first_data.count(), 1000);
    }
}
