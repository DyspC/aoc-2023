use std::cmp::Ordering;
use std::error::Error;
use std::iter::zip;

use itertools::Itertools;
use log::debug;

use aoc_2023::start;

const DAY: &str = module_path!();

/// Because the journey will take a few days, she offers to teach you the game of Camel Cards.
/// Camel Cards is sort of similar to poker except it's designed to be easier to play while riding a camel.
///
/// In Camel Cards, you get a list of hands, and your goal is to order them based on the strength of each hand.
/// A hand consists of five cards labeled one of A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, or 2.
/// The relative strength of each card follows this order, where A is the highest and 2 is the lowest.
///
/// Every hand is exactly one type.
/// From strongest to weakest, they are:
///
/// - Five of a kind, where all five cards have the same label: AAAAA
/// - Four of a kind, where four cards have the same label and one card has a different label: AA8AA
/// - Full house, where three cards have the same label, and the remaining two cards share a different label: 23332
/// - Three of a kind, where three cards have the same label, and the remaining two cards are each different from any other card in the hand: TTT98
/// - Two pair, where two cards share one label, two other cards share a second label, and the remaining card has a third label: 23432
/// - One pair, where two cards share one label, and the other three cards have a different label from the pair and each other: A23A4
/// - High card, where all cards' labels are distinct: 23456
///
/// Hands are primarily ordered based on type; for example, every full house is stronger than any three of a kind.
///
/// If two hands have the same type, a second ordering rule takes effect.
/// Start by comparing the first card in each hand.
/// If these cards are different, the hand with the stronger first card is considered stronger.
/// If the first card in each hand have the same label, however, then move on to considering the second card in each hand.
/// If they differ, the hand with the higher second card wins; otherwise, continue with the third card in each hand, then the fourth, then the fifth.
///
/// So, 33332 and 2AAAA are both four of a kind hands, but 33332 is stronger because its first card is stronger.
/// Similarly, 77888 and 77788 are both a full house, but 77888 is stronger because its third card is stronger (and both hands have the same first and second card).
///
/// To play Camel Cards, you are given a list of hands and their corresponding bid (your puzzle input).
///
/// For example:
///
/// ```
/// 32T3K 765
/// T55J5 684
/// KK677 28
/// KTJJT 220
/// QQQJA 483
/// ```
///
/// This example shows five hands; each hand is followed by its bid amount.
/// Each hand wins an amount equal to its bid multiplied by its rank, where the weakest hand gets rank 1, the second-weakest hand gets rank 2, and so on up to the strongest hand.
/// Because there are five hands in this example, the strongest hand will have rank 5 and its bid will be multiplied by 5.
///
/// So, the first step is to put the hands in order of strength:
///
/// - 32T3K is the only one pair and the other hands are all a stronger type, so it gets rank 1.
/// - KK677 and KTJJT are both two pair. Their first cards both have the same label, but the second card of KK677 is stronger (K vs T), so KTJJT gets rank 2 and KK677 gets rank 3.
/// - T55J5 and QQQJA are both three of a kind. QQQJA has a stronger first card, so it gets rank 5 and T55J5 gets rank 4.
///
/// Now, you can determine the total winnings of this set of hands by adding up the result of multiplying each hand's bid with its rank (765 * 1 + 220 * 2 + 28 * 3 + 684 * 4 + 483 * 5).
/// So the total winnings in this example are 6440.
///
/// Find the rank of every hand in your set.
/// What are the total winnings?
///
/// --- Part Two ---
///
/// To make things a little more interesting, the Elf introduces one additional rule.
/// Now, J cards are jokers - wildcards that can act like whatever card would make the hand the strongest type possible.
///
/// To balance this, J cards are now the weakest individual cards, weaker even than 2.
/// The other cards stay in the same order: A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J.
///
/// J cards can pretend to be whatever card is best for the purpose of determining hand type; for example, QJJQ2 is now considered four of a kind.
/// However, for the purpose of breaking ties between two hands of the same type, J is always treated as J, not the card it's pretending to be: JKKK2 is weaker than QQQQ2 because J is weaker than Q.
///
/// Now, the above example goes very differently:
///
/// ```
/// 32T3K 765
/// T55J5 684
/// KK677 28
/// KTJJT 220
/// QQQJA 483
/// ```
///
/// - 32T3K is still the only one pair; it doesn't contain any jokers, so its strength doesn't increase.
/// - KK677 is now the only two pair, making it the second-weakest hand.
/// - T55J5, KTJJT, and QQQJA are now all four of a kind! T55J5 gets rank 3, QQQJA gets rank 4, and KTJJT gets rank 5.
///
/// With the new joker rule, the total winnings in this example are 5905.
///
/// Using the new joker rule, find the rank of every hand in your set. What are the new total winnings?
///
/// @see https://adventofcode.com/2023/day/7
fn main() -> Result<(), Box<dyn Error>> {
    let mut hands: Vec<_> = start(DAY)?
        .filter_map(|l| CamelHand::parse(l.unwrap(), &Part::P2))
        .collect();
    hands.sort();
    hands.iter().for_each(|hand| debug!("{:?}", hand));
    let total: usize = hands
        .iter()
        .enumerate()
        .map(|(factor, hand)| (factor + 1) * hand.bid)
        .sum();
    println!("Summing to {}", total);
    Ok(())
}

enum Part {
    P1,
    P2,
}

#[derive(Debug, Eq, PartialEq)]
struct CamelHand {
    bid: usize,
    hand: Vec<CamelCard>,
    class: HandClass,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Hash)]
enum CamelCard {
    Jester,
    _2,
    _3,
    _4,
    _5,
    _6,
    _7,
    _8,
    _9,
    T,
    Jack,
    Q,
    K,
    A,
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum HandClass {
    HighCard,
    Pair,
    TwoPairs,
    Three,
    Full,
    FoOAK,
    FOAK,
}

impl HandClass {
    fn for_count(n: usize) -> Self {
        match n {
            5 => HandClass::FOAK,
            4 => HandClass::FoOAK,
            3 => HandClass::Three,
            2 => HandClass::Pair,
            1 => HandClass::HighCard,
            _ => unreachable!(),
        }
    }
}

impl CamelCard {
    fn from(ch: char, part: &Part) -> Self {
        match ch {
            '2' => CamelCard::_2,
            '3' => CamelCard::_3,
            '4' => CamelCard::_4,
            '5' => CamelCard::_5,
            '6' => CamelCard::_6,
            '7' => CamelCard::_7,
            '8' => CamelCard::_8,
            '9' => CamelCard::_9,
            'T' => CamelCard::T,
            'J' => match part {
                Part::P1 => CamelCard::Jack,
                Part::P2 => CamelCard::Jester,
            },
            'Q' => CamelCard::Q,
            'K' => CamelCard::K,
            'A' => CamelCard::A,
            ch => panic!("Unexpected {} as card value", ch),
        }
    }
}

impl CamelHand {
    fn parse(line: String, part: &Part) -> Option<Self> {
        let v: Vec<&str> = line.splitn(2, ' ').collect();
        if v.len() != 2 {
            return None;
        }
        let hand: Vec<CamelCard> = v[0].chars().map(|c| CamelCard::from(c, part)).collect();
        let bid: usize = v[1].parse().expect("Number as bid");

        let sortable: Vec<_> = hand.iter().clone().collect();
        let mut class = HandClass::HighCard;
        let map = sortable.iter().counts_by(|&c| c);
        for count in map.values() {
            match count {
                &c if c == 4 || c == 5 => {
                    class = HandClass::for_count(c);
                }
                3 => {
                    class = if class == HandClass::Pair {
                        HandClass::Full
                    } else {
                        HandClass::Three
                    }
                }
                2 => {
                    class = if class == HandClass::Three {
                        HandClass::Full
                    } else if class == HandClass::Pair {
                        HandClass::TwoPairs
                    } else {
                        HandClass::Pair
                    }
                }
                _ => {}
            }
        }

        let class = match part {
            Part::P1 => class,
            Part::P2 => match (&class, map.get(&CamelCard::Jester).unwrap_or(&0)) {
                (_, 0) => class,
                (HandClass::FOAK, _) => class,
                (HandClass::HighCard, 1) => HandClass::Pair,
                (HandClass::Pair, 1) => HandClass::Three,
                (HandClass::Pair, 2) => HandClass::Three,
                (HandClass::TwoPairs, 1) => HandClass::Full,
                (HandClass::TwoPairs, 2) => HandClass::FoOAK,
                (HandClass::Three, 1) => HandClass::FoOAK,
                (HandClass::Three, 3) => HandClass::FoOAK,
                (HandClass::FoOAK, 1) => HandClass::FOAK,
                (HandClass::FoOAK, 4) => HandClass::FOAK,
                (HandClass::Full, &c) if c == 2 || c == 3 => HandClass::FOAK,
                other => panic!("Unmapped jester transform {:?}", other),
            },
        };

        Some(Self { bid, hand, class })
    }
}

// Can't derive?
impl PartialOrd<Self> for CamelHand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for CamelHand {
    fn cmp(&self, other: &Self) -> Ordering {
        let class_order = self.class.cmp(&other.class);
        if class_order != Ordering::Equal {
            class_order
        } else if let Some((a, b)) = zip(&self.hand, &other.hand).find(|(a, b)| a.ne(b)) {
            a.cmp(b)
        } else {
            Ordering::Equal
        }
    }
}

#[cfg(test)]
mod tests {
    use log::debug;

    use aoc_2023::challenge_data;

    use crate::{CamelHand, Part, DAY};

    #[test]
    fn part1() {
        let mut hands: Vec<_> = challenge_data(DAY)
            .unwrap()
            .filter_map(|l| CamelHand::parse(l.unwrap(), &Part::P1))
            .collect();
        hands.sort();
        hands.iter().for_each(|hand| debug!("{:?}", hand));
        let total: usize = hands
            .iter()
            .enumerate()
            .map(|(factor, &ref hand)| (factor + 1) * hand.bid)
            .sum();
        assert_eq!(251029473, total);
    }

    #[test]
    fn part2() {
        let mut hands: Vec<_> = challenge_data(DAY)
            .unwrap()
            .filter_map(|l| CamelHand::parse(l.unwrap(), &Part::P2))
            .collect();
        hands.sort();
        hands.iter().for_each(|hand| debug!("{:?}", hand));
        let total: usize = hands
            .iter()
            .enumerate()
            .map(|(factor, &ref hand)| (factor + 1) * hand.bid)
            .sum();
        assert_eq!(251003917, total);
    }
}
