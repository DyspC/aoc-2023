use std::error::Error;

use aoc_2023::start;

const DAY: &str = module_path!();

/// As they're making the final adjustments, they discover that their calibration document (your puzzle input) has been amended by a very young Elf who was apparently just excited to show off her art skills.
/// Consequently, the Elves are having trouble reading the values on the document.
///
/// The newly-improved calibration document consists of lines of text;
/// each line originally contained a specific calibration value that the Elves now need to recover.
/// On each line, the calibration value can be found by combining the first digit and the last digit (in that order) to form a single two-digit number.
///
/// For example:
///
/// - `1abc2`
/// - `pqr3stu8vwx`
/// - `a1b2c3d4e5f`
/// - `treb7uchet`
///
/// In this example, the calibration values of these four lines are 12, 38, 15, and 77. Adding these together produces 142.
///
/// Consider your entire calibration document. What is the sum of all of the calibration values?
///
/// --- Part Two ---
///
/// Your calculation isn't quite right.
/// It looks like some of the digits are actually spelled out with letters: one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".
///
/// Equipped with this new information, you now need to find the real first and last digit on each line. For example:
///
/// - `two1nine`
/// - `eightwothree`
/// - `abcone2threexyz`
/// - `xtwone3four`
/// - `4nineeightseven2`
/// - `zoneight234`
/// - `7pqrstsixteen`
/// In this example, the calibration values are 29, 83, 13, 24, 42, 14, and 76. Adding these together produces 281.
///
/// What is the sum of all of the calibration values?
/// @see https://adventofcode.com/2023/day/1
fn main() -> Result<(), Box<dyn Error>> {
    let sum: usize = start(DAY)?.map(|l| numbers_p2(l.unwrap())).sum();
    println!("Summing to {}", sum);
    Ok(())
}

const DIGIT_NAMES: [(&str, usize); 9] = [
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
];

#[allow(dead_code)]
fn numbers_p1(line: String) -> usize {
    let predicate = |char: char| char.is_ascii_digit();
    if let Some(tens_index) = line.find(predicate) {
        let units_index = line
            .rfind(predicate)
            .expect("Should have hit tens_index as fallback");
        let tens: usize = String::from_utf8_lossy(&[line.as_bytes()[tens_index]])
            .parse()
            .unwrap();
        let units: usize = String::from_utf8_lossy(&[line.as_bytes()[units_index]])
            .parse()
            .unwrap();
        tens * 10 + units
    } else {
        0
    }
}

fn numbers_p2(line: String) -> usize {
    let mut left_idx: Option<usize> = None;
    let mut left_val: Option<usize> = None;
    let mut right_idx: Option<usize> = None;
    let mut right_val: Option<usize> = None;
    for (repr, val) in DIGIT_NAMES {
        if let Some(idx) = line.find(repr) {
            if !left_idx.iter().any(|&old| old < idx) {
                left_idx = Some(idx);
                left_val = Some(val);
            }
        }
        if let Some(idx) = line.rfind(repr) {
            if !right_idx.iter().any(|&old| idx < old) {
                right_idx = Some(idx);
                right_val = Some(val);
            }
        }
        let string = format!("{}", val);
        let digit = string.as_str();
        if let Some(idx) = line.find(digit) {
            if !left_idx.iter().any(|&old| old < idx) {
                left_idx = Some(idx);
                left_val = Some(val);
            }
        }
        if let Some(idx) = line.rfind(digit) {
            if !right_idx.iter().any(|&old| idx < old) {
                right_idx = Some(idx);
                right_val = Some(val);
            }
        }
    }
    match (left_val, right_val) {
        (None, None) => 0,
        (Some(d), Some(u)) => 10 * d + u,
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use aoc_2023::challenge_data;

    use super::{numbers_p1, numbers_p2, DAY};

    #[test]
    fn part1() {
        let sum: usize = challenge_data(DAY)
            .unwrap()
            .map(|l| numbers_p1(l.unwrap()))
            .sum();
        assert_eq!(54331, sum);
    }

    #[test]
    fn part2() {
        let sum: usize = challenge_data(DAY)
            .unwrap()
            .map(|l| numbers_p2(l.unwrap()))
            .sum();
        assert_eq!(54518, sum);
    }
}
