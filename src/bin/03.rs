use std::cell::RefCell;
use std::cmp::min;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::Lines;

use log::{debug, info, trace};

use aoc_2023::start;

const DAY: &str = module_path!();

/// The engineer explains that an engine part seems to be missing from the engine, but nobody can figure out which one.
/// If you can add up all the part numbers in the engine schematic, it should be easy to work out which part is missing.
///
/// The engine schematic (your puzzle input) consists of a visual representation of the engine.
/// There are lots of numbers and symbols you don't really understand, but apparently any number adjacent to a symbol, even diagonally, is a "part number" and should be included in your sum.
/// (Periods (.) do not count as a symbol.)
///
/// Here is an example engine schematic:
///
/// ```
/// 467..114..
/// ...*......
/// ..35..633.
/// ......#...
/// 617*......
/// .....+.58.
/// ..592.....
/// ......755.
/// ...$.*....
/// .664.598..
/// ```
/// In this schematic, two numbers are not part numbers because they are not adjacent to a symbol: 114 (top right) and 58 (middle right).
/// Every other number is adjacent to a symbol and so is a part number; their sum is 4361.
///
/// Of course, the actual engine schematic is much larger.
/// What is the sum of all of the part numbers in the engine schematic?
///
/// --- Part Two ---
///
/// The engineer finds the missing part and installs it in the engine!
/// As the engine springs to life, you jump in the closest gondola, finally ready to ascend to the water source.
///
/// You don't seem to be going very fast, though.
/// Maybe something is still wrong?
/// Fortunately, the gondola has a phone labeled "help", so you pick it up and the engineer answers.
///
/// Before you can explain the situation, she suggests that you look out the window.
/// There stands the engineer, holding a phone in one hand and waving with the other.
/// You're going so slowly that you haven't even left the station.
/// You exit the gondola.
///
/// The missing part wasn't the only issue - one of the gears in the engine is wrong.
/// A gear is any * symbol that is adjacent to exactly two part numbers.
/// Its gear ratio is the result of multiplying those two numbers together.
///
/// This time, you need to find the gear ratio of every gear and add them all up so that the engineer can figure out which gear needs to be replaced.
///
/// Consider the same engine schematic again:
///
/// ```
/// 467..114..
/// ...*......
/// ..35..633.
/// ......#...
/// 617*......
/// .....+.58.
/// ..592.....
/// ......755.
/// ...$.*....
/// .664.598..
/// ```
///
/// In this schematic, there are two gears.
/// The first is in the top left; it has part numbers 467 and 35, so its gear ratio is 16345.
/// The second gear is in the lower right; its gear ratio is 451490.
/// (The * adjacent to 617 is not a gear because it is only adjacent to one part number.)
/// Adding up all of the gear ratios produces 467835.
///
/// @see https://adventofcode.com/2023/day/3
fn main() -> Result<(), Box<dyn Error>> {
    let lines = start(DAY)?;
    let (numbers, symbols) = tokens(lines);
    let sum = clong_clong(numbers, symbols);
    info!("Summed to {}", sum);
    Ok(())
}

fn clong(numbers: Vec<NumberBox>, symbols: Vec<SymbolBox>) -> usize {
    numbers
        .iter()
        .filter_map(|nb: &NumberBox| {
            if symbols.iter().any(|sb| sb.adjacent(nb)) {
                debug!("Matched number {:?}", nb);
                Some(nb.number)
            } else {
                None
            }
        })
        .sum()
}

fn clong_clong(numbers: Vec<NumberBox>, symbols: Vec<SymbolBox>) -> usize {
    symbols
        .iter()
        .filter(|sb| sb.symbol == '*')
        .filter_map(|sb| {
            let adjs: Vec<&NumberBox> = numbers.iter().filter(|nb| sb.adjacent(nb)).collect();
            if adjs.len() == 2 {
                Some((adjs.get(0).unwrap().number, adjs.get(1).unwrap().number))
            } else {
                None
            }
        })
        .map(|(w1, w2)| w1 * w2)
        .sum()
}

#[derive(Debug)]
struct SymbolBox {
    i: usize,
    j: usize,
    symbol: char,
}

impl SymbolBox {
    fn new(i: usize, j: usize, symbol: char) -> Self {
        SymbolBox { i, j, symbol }
    }

    fn adjacent(&self, number_box: &NumberBox) -> bool {
        let ret = self.i - min(self.i, 1) <= number_box.i
            && number_box.i <= self.i + 1
            && number_box.j - min(number_box.j, 1) <= self.j
            && self.j <= number_box.j + number_box.length;
        if ret {
            trace!("Matched symbol {:?} to {:?}", self, number_box);
        }
        ret
    }
}

struct NumberContinuation {
    i: usize,
    j: usize,
    repr: RefCell<String>,
}

impl NumberContinuation {
    fn new(i: usize, j: usize, repr: String) -> Self {
        NumberContinuation {
            i,
            j,
            repr: RefCell::new(repr),
        }
    }

    fn extend(&mut self, c: char) {
        self.repr.get_mut().push(c);
    }
}

#[derive(Debug)]
struct NumberBox {
    i: usize,
    j: usize,
    length: usize,
    number: usize,
}

impl From<NumberContinuation> for NumberBox {
    fn from(value: NumberContinuation) -> Self {
        let repr = value.repr.take();
        Self {
            i: value.i,
            j: value.j,
            length: repr.len(),
            number: repr.parse().unwrap(),
        }
    }
}

fn tokens(lines: Lines<BufReader<File>>) -> (Vec<NumberBox>, Vec<SymbolBox>) {
    let mut numbers: Vec<NumberBox> = vec![];
    let mut symbols: Vec<SymbolBox> = vec![];
    let mut i = 1;
    for res in lines {
        let line = res.unwrap();
        let mut number_continuation: Option<NumberContinuation> = None;
        for (j, c) in line.char_indices() {
            match c {
                '.' => {
                    trace!("{}\t{}\t DOT", i, j);
                    if number_continuation.is_some() {
                        let number_box = number_continuation.take().unwrap().into();
                        debug!("Created {:?}", number_box);
                        numbers.push(number_box);
                    }
                }
                ch if ch.is_ascii_digit() => {
                    if number_continuation.is_some() {
                        trace!("{}\t{}\t DIGIT {} continued", i, j, ch);
                        // Break the option update in loop scam
                        let mut nc = number_continuation.take().unwrap();
                        nc.extend(ch);
                        let _ = number_continuation.insert(nc);
                    } else {
                        trace!("{}\t{}\t DIGIT {} new", i, j, ch);
                        number_continuation = Some(NumberContinuation::new(i, j, String::from(ch)));
                    }
                }
                ch => {
                    trace!("{}\t{}\t SYMBOL {}", i, j, ch);
                    let symbol_box = SymbolBox::new(i, j, ch);
                    debug!("Created {:?}", symbol_box);
                    symbols.push(symbol_box);
                    if number_continuation.is_some() {
                        let number_box = number_continuation.take().unwrap().into();
                        debug!("Created {:?}", number_box);
                        numbers.push(number_box);
                    }
                }
            }
        }
        if number_continuation.is_some() {
            let number_box = number_continuation.take().unwrap().into();
            debug!("Created {:?}", number_box);
            numbers.push(number_box);
        }
        i += 1;
    }
    (numbers, symbols)
}

#[cfg(test)]
mod tests {
    use aoc_2023::challenge_data;

    use crate::{clong, clong_clong, tokens, DAY};

    #[test]
    fn part1() {
        let lines = challenge_data(DAY).unwrap();
        let (numbers, symbols) = tokens(lines);
        assert_eq!(531932, clong(numbers, symbols));
    }

    #[test]
    fn part2() {
        let lines = challenge_data(DAY).unwrap();
        let (numbers, symbols) = tokens(lines);
        assert_eq!(73646890, clong_clong(numbers, symbols));
    }
}
