use std::collections::VecDeque;
use std::error::Error;

use log::debug;

use aoc_2023::start;

const DAY: &str = module_path!();

/// You pull out your handy Oasis And Sand Instability Sensor and analyze your surroundings. The OASIS produces a report of many values and how they are changing over time (your puzzle input). Each line in the report contains the history of a single value. For example:
///
/// ```
/// 0 3 6 9 12 15
/// 1 3 6 10 15 21
/// 10 13 16 21 30 45
/// ```
///
/// To best protect the oasis, your environmental report should include a prediction of the next value in each history.
/// To do this, start by making a new sequence from the difference at each step of your history.
/// If that sequence is not all zeroes, repeat this process, using the sequence you just generated as the input sequence.
/// Once all of the values in your latest sequence are zeroes, you can extrapolate what the next value of the original history should be.
///
/// In the above dataset, the first history is 0 3 6 9 12 15.
/// Because the values increase by 3 each step, the first sequence of differences that you generate will be 3 3 3 3 3.
/// Note that this sequence has one fewer value than the input sequence because at each step it considers two numbers from the input.
/// Since these values aren't all zero, repeat the process: the values differ by 0 at each step, so the next sequence is 0 0 0 0.
/// This means you have enough information to extrapolate the history!
/// Visually, these sequences can be arranged like this:
///
/// ```
/// 0   3   6   9  12  15
///   3   3   3   3   3
///     0   0   0   0
/// ```
///
/// To extrapolate, start by adding a new zero to the end of your list of zeroes; because the zeroes represent differences between the two values above them, this also means there is now a placeholder in every sequence above it:
///
/// ```
/// 0   3   6   9  12  15   B
///   3   3   3   3   3   A
///     0   0   0   0   0
/// ```
///
/// You can then start filling in placeholders from the bottom up.
/// A needs to be the result of increasing 3 (the value to its left) by 0 (the value below it); this means A must be 3:
///
/// ```
/// 0   3   6   9  12  15   B
///   3   3   3   3   3   3
///     0   0   0   0   0
/// ```
///
/// Finally, you can fill in B, which needs to be the result of increasing 15 (the value to its left) by 3 (the value below it), or 18:
///
/// ```
/// 0   3   6   9  12  15  18
///   3   3   3   3   3   3
///     0   0   0   0   0
/// ```
///
/// So, the next value of the first history is 18.
///
/// Finding all-zero differences for the second history requires an additional sequence:
///
/// ```
/// 1   3   6  10  15  21
///   2   3   4   5   6
///     1   1   1   1
///       0   0   0
/// ```
///
/// Then, following the same process as before, work out the next value in each sequence from the bottom up:
///
/// ```
/// 1   3   6  10  15  21  28
///   2   3   4   5   6   7
///     1   1   1   1   1
///       0   0   0   0
/// ```
///
/// So, the next value of the second history is 28.
///
/// The third history requires even more sequences, but its next value can be found the same way:
///
/// ```
/// 10  13  16  21  30  45  68
///    3   3   5   9  15  23
///      0   2   4   6   8
///        2   2   2   2
///          0   0   0
/// ```
///
/// So, the next value of the third history is 68.
///
/// If you find the next value for each history in this example and add them together, you get 114.
///
/// Analyze your OASIS report and extrapolate the next value for each history.
/// What is the sum of these extrapolated values?
///
/// --- Part Two ---
///
/// Of course, it would be nice to have even more history included in your report.
/// Surely it's safe to just extrapolate backwards as well, right?
///
/// For each history, repeat the process of finding differences until the sequence of differences is entirely zero.
/// Then, rather than adding a zero to the end and filling in the next values of each previous sequence, you should instead add a zero to the beginning of your sequence of zeroes, then fill in new first values for each previous sequence.
///
/// In particular, here is what the third example history looks like when extrapolating back in time:
///
/// ```
/// 5  10  13  16  21  30  45
///   5   3   3   5   9  15
///    -2   0   2   4   6
///       2   2   2   2
///         0   0   0
/// ```
///
/// Adding the new values on the left side of each sequence from bottom to top eventually reveals the new left-most history value: 5.
///
/// Doing this for the remaining example data above results in previous values of -3 for the first history and 0 for the second history.
/// Adding all three new values together produces 2.
///
/// Analyze your OASIS report again, this time extrapolating the previous value for each history.
/// What is the sum of these extrapolated values?
///
/// @see https://adventofcode.com/2023/day/9
fn main() -> Result<(), Box<dyn Error>> {
    let data: VecDeque<String> = start(DAY)?.map_while(Result::ok).collect();

    let probes = parse_probes(data);

    let result: isize = probes
        .iter()
        .map(|probe| {
            debug!("Read line {:?}", probe);
            head_interpolation(probe)
        })
        .sum();

    print!("Result is {}", result);

    Ok(())
}

fn parse_probes(data: VecDeque<String>) -> Vec<Vec<isize>> {
    data.iter()
        .map(|string| {
            string
                .split_ascii_whitespace()
                .filter(|string| !string.is_empty())
                .map(|string| string.parse::<isize>())
                .map_while(Result::ok)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<_>>>()
}

fn tail_interpolation(probe: &Vec<isize>) -> isize {
    let mut it = probe.clone();
    let mut sum = 0;
    while it.iter().any(|&i| i != 0) {
        sum += it.last().expect("empty data");
        it = it
            .windows(2)
            .map(|w| match &w {
                &[e1, e2] => e2 - e1,
                _ => unreachable!(),
            })
            .collect();
    }
    if it.is_empty() {
        panic!("Did not reach the full 0 state");
    }
    debug!("The next value of {:?} is {}", probe, sum);
    sum
}

fn head_interpolation(probe: &Vec<isize>) -> isize {
    let mut it = probe.clone();
    let mut sum = 0;
    let mut factor = 1;
    while it.iter().any(|&i| i != 0) {
        sum += factor * it.first().expect("empty data");
        factor *= -1;
        it = it
            .windows(2)
            .map(|w| match &w {
                &[e1, e2] => e2 - e1,
                _ => unreachable!(),
            })
            .collect();
    }
    if it.is_empty() {
        panic!("Did not reach the full 0 state");
    }
    debug!("The next value of {:?} is {}", probe, sum);
    sum
}

#[cfg(test)]
mod tests {
    use std::collections::VecDeque;

    use aoc_2023::challenge_data;

    use crate::{head_interpolation, parse_probes, tail_interpolation, DAY};

    #[test]
    fn part1() {
        let data: VecDeque<String> = challenge_data(DAY).unwrap().map_while(Result::ok).collect();

        let probes = parse_probes(data);

        let result: isize = probes.iter().map(|probe| tail_interpolation(probe)).sum();
        assert_eq!(1637452029, result);
    }

    #[test]
    fn part2() {
        let data: VecDeque<String> = challenge_data(DAY).unwrap().map_while(Result::ok).collect();

        let probes = parse_probes(data);

        let result: isize = probes.iter().map(|probe| head_interpolation(probe)).sum();
        assert_eq!(908, result);
    }
}
