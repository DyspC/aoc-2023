use std::collections::hash_map::Entry::Vacant;
use std::collections::{HashMap, VecDeque};
use std::error::Error;

use itertools::Itertools;
use log::{debug, error, info, trace};
use num::integer::lcm;

use aoc_2023::start;

const DAY: &str = module_path!();

/// It seems like you're meant to use the left/right instructions to navigate the network.
/// Perhaps if you have the camel follow the same instructions, you can escape the haunted wasteland!
///
/// After examining the maps for a bit, two nodes stick out: AAA and ZZZ.
/// You feel like AAA is where you are now, and you have to follow the left/right instructions until you reach ZZZ.
///
/// This format defines each node of the network individually. For example:
///
/// ```
/// RL
///
/// AAA = (BBB, CCC)
/// BBB = (DDD, EEE)
/// CCC = (ZZZ, GGG)
/// DDD = (DDD, DDD)
/// EEE = (EEE, EEE)
/// GGG = (GGG, GGG)
/// ZZZ = (ZZZ, ZZZ)
/// ```
///
/// Starting with AAA, you need to look up the next element based on the next left/right instruction in your input.
/// In this example, start with AAA and go right (R) by choosing the right element of AAA, CCC.
/// Then, L means to choose the left element of CCC, ZZZ.
/// By following the left/right instructions, you reach ZZZ in 2 steps.
///
/// Of course, you might not find ZZZ right away.
/// If you run out of left/right instructions, repeat the whole sequence of instructions as necessary: RL really means RLRLRLRLRLRLRLRL... and so on.
/// For example, here is a situation that takes 6 steps to reach ZZZ:
///
/// ```
/// LLR
///
/// AAA = (BBB, BBB)
/// BBB = (AAA, ZZZ)
/// ZZZ = (ZZZ, ZZZ)
/// ```
///
/// Starting at AAA, follow the left/right instructions.
/// How many steps are required to reach ZZZ?
///
/// --- Part Two ---
///
/// The sandstorm is upon you and you aren't any closer to escaping the wasteland.
/// You had the camel follow the instructions, but you've barely left your starting position.
/// It's going to take significantly more steps to escape!
///
/// What if the map isn't for people - what if the map is for ghosts?
/// Are ghosts even bound by the laws of spacetime?
/// Only one way to find out.
///
/// After examining the maps a bit longer, your attention is drawn to a curious fact: the number of nodes with names ending in A is equal to the number ending in Z!
/// If you were a ghost, you'd probably just start at every node that ends with A and follow all of the paths at the same time until they all simultaneously end up at nodes that end with Z.
///
/// For example:
///
/// ```
/// LR
///
/// 11A = (11B, XXX)
/// 11B = (XXX, 11Z)
/// 11Z = (11B, XXX)
/// 22A = (22B, XXX)
/// 22B = (22C, 22C)
/// 22C = (22Z, 22Z)
/// 22Z = (22B, 22B)
/// XXX = (XXX, XXX)
/// ```
///
/// Here, there are two starting nodes, 11A and 22A (because they both end with A). As you follow each left/right instruction, use that instruction to simultaneously navigate away from both nodes you're currently on. Repeat this process until all of the nodes you're currently on end with Z. (If only some of the nodes you're on end with Z, they act like any other node and you continue as normal.) In this example, you would proceed as follows:
///
/// - Step 0: You are at 11A and 22A.
/// - Step 1: You choose all of the left paths, leading you to 11B and 22B.
/// - Step 2: You choose all of the right paths, leading you to 11Z and 22C.
/// - Step 3: You choose all of the left paths, leading you to 11B and 22Z.
/// - Step 4: You choose all of the right paths, leading you to 11Z and 22B.
/// - Step 5: You choose all of the left paths, leading you to 11B and 22C.
/// - Step 6: You choose all of the right paths, leading you to 11Z and 22Z.
/// So, in this example, you end up entirely on nodes that end in Z after 6 steps.
///
/// Simultaneously start on every node that ends with A. How many steps does it take before you're only on nodes that end with Z?
///
/// @see https://adventofcode.com/2023/day/8
fn main() -> Result<(), Box<dyn Error>> {
    let data: VecDeque<String> = start(DAY)?.map_while(Result::ok).collect();

    let (instructions, edges) = parse_data(data);

    let ops = p2(instructions, edges);
    print!("Found all xxZ in {} steps", ops);

    Ok(())
}

fn p1(instructions: Vec<Instruction>, edges: HashMap<String, Vertex>) -> usize {
    let mut current = "AAA".to_string();
    let mut walker = instructions.iter().cycle().enumerate();
    loop {
        let (ops, inst) = walker.next().unwrap();
        if current == "ZZZ" {
            break ops;
        } else if ops > 10_usize.pow(9) {
            panic!("???");
        }
        let vertex = edges
            .get(&current)
            .unwrap_or_else(|| panic!("Missing node {}", &current));
        trace!("# {} - {} [{:?}] = {:?}", ops, current, inst, vertex);
        current = match inst {
            Instruction::Left => vertex.left.to_owned(),
            Instruction::Right => vertex.right.to_string(),
        };
    }
}

// fn p2_naive(instructions: Vec<Instruction>, edges: HashMap<String, Vertex>) -> usize {
//     let mut current = edges.keys()
//         .filter(|&s| s.ends_with("A"))
//         .cloned()
//         .collect_vec();
//     let mut walker = instructions.iter().cycle().enumerate();
//     loop {
//         let (ops, inst) = walker.next().unwrap();
//         if current.iter().all(|s| {
//             s.ends_with("Z")
//         }) {
//             break ops;
//         } else if ops > 10_usize.pow(15) {
//             panic!("???");
//         }
//
//         for idx in 0..current.len() {
//             let c = &current[idx];
//             let vertex = edges.get(c).unwrap();
//             trace!("# {} - {} [{:?}] = {:?}", ops, c, inst, vertex);
//             let dst = match inst {
//                 Instruction::Left => vertex.left.to_owned(),
//                 Instruction::Right => vertex.right.to_owned(),
//             };
//             if dst.ends_with("Z") {
//                 debug!("Matched [{}] on {} at {}", idx, dst, ops);
//             }
//             current[idx] = dst;
//         }
//     }
// }

fn p2(instructions: Vec<Instruction>, edges: HashMap<String, Vertex>) -> usize {
    let hard_limit = instructions.len() * edges.len();
    info!("Exploration hard limit is {}", hard_limit);
    let mut current = edges
        .keys()
        .filter(|&s| s.ends_with('A'))
        .cloned()
        .collect_vec();
    let mut looping: HashMap<usize, VecDeque<(usize, String)>> = current
        .iter()
        .enumerate()
        .map(|(i, _)| (i, VecDeque::new()))
        .collect();
    let mut loop_info: HashMap<usize, (usize, usize)> = HashMap::new(); // (offset, length)
    let mut matching: HashMap<usize, VecDeque<usize>> = current
        .iter()
        .enumerate()
        .map(|(i, _)| (i, VecDeque::new()))
        .collect();
    let mut walker = instructions.iter().cycle().enumerate();
    while loop_info.len() < current.len() {
        let (ops, inst) = walker.next().unwrap();

        if hard_limit < ops {
            error!("Hit hard limit");
            break;
        }

        for idx in 0..current.len() {
            let c = current[idx].to_string();
            let vertex = edges.get(&c).unwrap();
            // trace!("# {} - {} [{:?}] = {:?}", ops, c, inst, vertex);
            if let Vacant(e) = loop_info.entry(idx) {
                let hist_entry = (ops % instructions.len(), c.clone());
                let history = looping.get_mut(&idx).unwrap();
                if let Some(seen) = history
                    .iter()
                    .position(|candidate| candidate == &hist_entry)
                {
                    e.insert((seen, history.len() - seen));
                    looping.remove(&idx);
                } else {
                    history.push_back(hist_entry);
                }
                if c.ends_with('Z') {
                    debug!("Matched [{}] on {} at {}", idx, c, ops);
                    let matches = matching.get_mut(&idx).unwrap();
                    matches.push_back(ops);
                }
            }

            let dst = match inst {
                Instruction::Left => vertex.left.to_owned(),
                Instruction::Right => vertex.right.to_owned(),
            };

            current[idx] = dst;
        }
    }

    info!("Found looping spans {:?}", loop_info);
    info!("Found matching spans {:?}", matching);

    let lcm = loop_info
        .values()
        .fold(1_usize, |acc, &(_, curr)| lcm(acc, curr));
    info!("LCM = {}", lcm);
    return lcm;

    /*
    // this LCM processing being correct would have saved a fuck ton of time
    let fst = loop_info.values().find(|_| true).unwrap();
    let (prd, pgcd) = loop_info
        .values()
        .fold((BigUint::one(), fst.1), |acc, (_, curr)| {
            (acc.0 * curr, euclide(&acc.1, curr))
        });
    // info!("Loops accumulate to prod={}, PGCD={}", prd, pgcd);
    let ppcm = prd / pgcd;
    info!("hard limit = {}", ppcm);

    let mut generators: Vec<AffineGenerator> = vec![];
    for (idx, (_, length)) in loop_info {
        let mut anchors: Vec<usize> = vec![];
        for &x in matching.get(&idx).unwrap() {
            anchors.push(x)
        }
        let generator = AffineGenerator::multiple(anchors, length);
        debug!("Created generator {:?}", generator);
        generators.push(generator);
    }
    loop {
        let current = generators.iter().map(|g| g.current()).min().unwrap();
        let mut stop = true;
        for idx in 0..generators.len() {
            if current == generators[idx].current() {
                let n = generators[idx].next();
                trace!("Generator[{}] reached step {:?}", idx, n);
            } else {
                stop = false;
            }
        }
        if stop {
            info!("Reached the end");
            break current;
        } else if ppcm < BigUint::from(current) {
            // panic before the end of time
            panic!("byebye world");
        }
    }
    */
}

// fn euclide(a: &usize, b: &usize) -> usize {
//     let mut i = *a;
//     let mut j = *b;
//     while j > 0 {
//         let t = j;
//         j = i % j;
//         i = t;
//     }
//     i
// }

fn parse_data(mut data: VecDeque<String>) -> (Vec<Instruction>, HashMap<String, Vertex>) {
    let char_line = data.pop_front().unwrap();
    let chars = char_line.chars();
    let instructions = chars
        .map(|ch| match ch {
            'L' => Instruction::Left,
            'R' => Instruction::Right,
            _ => unreachable!(),
        })
        .collect::<Vec<_>>();

    let edges: HashMap<String, Vertex> = data
        .iter()
        .skip(1)
        .map(move |line| {
            let [name, left, right] = &line
                .chars()
                .filter(char::is_ascii_alphanumeric)
                .collect::<Vec<char>>()[..]
                .chunks(3)
                .map(move |win| win.iter().collect())
                .collect::<Vec<String>>()[0..3]
            else {
                unreachable!()
            };
            (
                name.into(),
                Vertex {
                    name: name.into(),
                    left: left.into(),
                    right: right.into(),
                },
            )
        })
        .collect();
    (instructions, edges)
}

#[derive(Debug)]
enum Instruction {
    Left,
    Right,
}

#[derive(Debug)]
struct Vertex {
    name: String,
    left: String,
    right: String,
}
//
// #[derive(Debug)]
// struct AffineGenerator {
//     step: usize,
//     current: Vec<Cell<usize>>,
// }
//
// impl AffineGenerator {
//     fn single(first: usize, step: usize) -> Self {
//         AffineGenerator {
//             step,
//             current: vec![Cell::new(first)],
//         }
//     }
//     fn multiple(first: Vec<usize>, step: usize) -> Self {
//         AffineGenerator {
//             step,
//             current: first.into_iter().map(Cell::new).collect(),
//         }
//     }
//
//     fn current(&self) -> usize {
//         self.current.iter().map(Cell::get).min().unwrap()
//     }
// }
//
// impl Iterator for AffineGenerator {
//     type Item = usize;
//
//     fn next(&mut self) -> Option<Self::Item> {
//         let x = self.current.iter().min_by_key(|&cell| cell.get()).unwrap();
//         let ret = x.replace(x.get() + self.step);
//         Some(ret)
//     }
// }

#[cfg(test)]
mod tests {
    use std::collections::VecDeque;

    use aoc_2023::challenge_data;

    use crate::{p1, p2, parse_data, DAY};

    #[test]
    fn part1() {
        let data: VecDeque<String> = challenge_data(DAY).unwrap().map_while(Result::ok).collect();

        let (instructions, edges) = parse_data(data);

        let ops = p1(instructions, edges);
        assert_eq!(16271, ops);
    }

    #[test]
    fn part2() {
        let data: VecDeque<String> = challenge_data(DAY).unwrap().map_while(Result::ok).collect();

        let (instructions, edges) = parse_data(data);

        let ops = p2(instructions, edges);
        assert_eq!(14265111103729, ops);
    }
}
