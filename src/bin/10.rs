use std::collections::HashSet;
use std::error::Error;

use log::{error, info, trace};

use aoc_2023::start;

const DAY: &str = module_path!();

/// Scanning the area, you discover that the entire field you're standing on is densely packed with pipes; it was hard to tell at first because they're the same metallic silver color as the "ground". You make a quick sketch of all of the surface pipes you can see (your puzzle input).
///
/// The pipes are arranged in a two-dimensional grid of tiles:
///
/// | is a vertical pipe connecting north and south.
/// - is a horizontal pipe connecting east and west.
/// L is a 90-degree bend connecting north and east.
/// J is a 90-degree bend connecting north and west.
/// 7 is a 90-degree bend connecting south and west.
/// F is a 90-degree bend connecting south and east.
/// . is ground; there is no pipe in this tile.
/// S is the starting position of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.
/// Based on the acoustics of the animal's scurrying, you're confident the pipe that contains the animal is one large, continuous loop.
///
/// For example, here is a square loop of pipe:
///
/// .....
/// .F-7.
/// .|.|.
/// .L-J.
/// .....
///
/// [... de la merde y a 100 lignes d'exemples go site pour la p1 ...]
///
/// How many steps along the loop does it take to get from the starting position to the point farthest from the starting position?
///
/// --- Part Two ---
///
/// You quickly reach the farthest point of the loop, but the animal never emerges.
/// Maybe its nest is within the area enclosed by the loop?
///
/// In fact, there doesn't even need to be a full tile path to the outside for tiles to count as outside the loop - squeezing between pipes is also allowed!
///
/// Any tile that isn't part of the main loop can count as being enclosed by the loop.
///
/// Figure out whether you have time to search for the nest by calculating the area within the loop.
/// How many tiles are enclosed by the loop?
///
/// @see https://adventofcode.com/2023/day/10
fn main() -> Result<(), Box<dyn Error>> {
    let data: Vec<String> = start(DAY)?.map_while(Result::ok).collect();

    let (tile_map, start) = parse_tile_map(data);

    let n = tile_map.len();
    let m = tile_map[0].len();

    info!("Parsed data with {} lines and {} cols", n, m);
    assert_eq!(Tile::Start, tile_map[start.0][start.1]);
    trace!("Valid coordinates for starting point");

    if let Some((length, edges)) = walk_loop(&tile_map, start) {
        info!("Looped through the map in {} movements", length);
        println!("Most distant tile of the loop is {} steps away", length / 2);

        // p2
        let interior_area = area(n, m, edges);
        println!("The interior area of the loop is {}", interior_area);
    } else {
        error!("Scrgngn");
    }

    Ok(())
}

fn area(n: usize, m: usize, edges: Vec<(usize, usize)>) -> usize {
    let mut swaps: Vec<Vec<bool>> = vec![];
    for idx in 0..n {
        let mut current = if let Some(last) = swaps.last() {
            last.to_vec()
        } else {
            [false].repeat(m)
        };
        for swap_idx in edges
            .iter()
            .filter_map(|&(i, j)| if i == idx { Some(j) } else { None })
        {
            current[swap_idx] = !current[swap_idx];
        }
        swaps.push(current);
    }
    let sets = swaps
        .iter()
        .map(|markers| {
            markers
                .iter()
                .enumerate()
                .filter_map(|(i, &v)| if v { Some(i) } else { None })
                .collect::<Vec<_>>()[..]
                .chunks(2)
                .flat_map(|c| match c {
                    &[min, max] => (min + 1)..max,
                    _ => unreachable!(),
                })
                .collect::<HashSet<usize>>()
        })
        .collect::<Vec<_>>();
    sets.windows(2)
        .map(|win| win[0].intersection(&win[1]).count())
        .sum()
}

/// Read the input and return the map and the starting coordinates
fn parse_tile_map(data: Vec<String>) -> (TileMap, (usize, usize)) {
    let mut res = vec![];
    let mut s_coords: Option<_> = None;
    for (i, line) in data.iter().enumerate() {
        let mut parsed_line = vec![];
        for (j, c) in line.chars().enumerate() {
            let tile: Tile = c.into();
            if tile == Tile::Start {
                if let Some(older) = s_coords.replace((i, j)) {
                    panic!(
                        "Found multiple starting points in {:?} and {:?}",
                        older,
                        (i, j)
                    )
                }
            }
            parsed_line.push(tile);
        }
        res.push(parsed_line);
    }
    (
        res,
        s_coords.expect("Did not find the starting point chan parsing input"),
    )
}

/// Explores the loop from the given coordinates and maybe returns the length of the first found loop
fn walk_loop(tile_map: &TileMap, start: (usize, usize)) -> Option<(usize, Vec<(usize, usize)>)> {
    let mut coords = start;

    let mut loop_size = 0;

    // Can an iterator return a non reference?
    let mut movement = if accept_movement(
        tile_map,
        Movement::Left.next_coords(start),
        &Movement::Left,
    )
    .is_some()
    {
        Movement::Left
    } else if accept_movement(
        tile_map,
        Movement::Right.next_coords(start),
        &Movement::Right,
    )
    .is_some()
    {
        Movement::Right
    } else if accept_movement(tile_map, Movement::Top.next_coords(start), &Movement::Top).is_some()
    {
        Movement::Top
    } else {
        panic!("Tested 3/4 starting directions and none was acceptable");
    };

    let mut edges: Vec<(usize, usize)> = vec![];

    loop {
        coords = movement.next_coords(coords);
        let next = accept_movement(tile_map, coords, &movement);
        loop_size += 1;
        if next.is_some() {
            if let Some((m, c)) = next {
                movement = m;
                if c {
                    edges.push(coords);
                }
                if movement == Movement::Stop {
                    break;
                }
            }
        }

        if loop_size > tile_map.len().pow(2) {
            panic!("Oupsi");
        }
    }

    if edges.len() % 2 == 1 {
        edges.push(start);
    }

    Some((loop_size, edges))
}

/// If the target coords can be entered from a direction, returns the next possible movement
fn accept_movement(
    tile_map: &TileMap,
    coords: (usize, usize),
    movement: &Movement,
) -> Option<(Movement, bool)> {
    if !(0..tile_map.len()).contains(&coords.0) {
        return None;
    }
    if !(0..tile_map[coords.0].len()).contains(&coords.1) {
        return None;
    }
    match (&tile_map[coords.0][coords.1], movement) {
        (Tile::Start, _) => Some((Movement::Stop, false)),
        (Tile::Null, _) => None,

        (Tile::Horizontal, Movement::Left) => Some((Movement::Left, false)),
        (Tile::Horizontal, Movement::Right) => Some((Movement::Right, false)),
        (Tile::Vertical, Movement::Top) => Some((Movement::Top, false)),
        (Tile::Vertical, Movement::Bottom) => Some((Movement::Bottom, false)),

        (Tile::TopRight, Movement::Bottom) => Some((Movement::Right, true)),
        (Tile::TopRight, Movement::Left) => Some((Movement::Top, true)),
        (Tile::BottomRight, Movement::Top) => Some((Movement::Right, true)),
        (Tile::BottomRight, Movement::Left) => Some((Movement::Bottom, true)),
        (Tile::TopLeft, Movement::Bottom) => Some((Movement::Left, true)),
        (Tile::TopLeft, Movement::Right) => Some((Movement::Top, true)),
        (Tile::BottomLeft, Movement::Top) => Some((Movement::Left, true)),
        (Tile::BottomLeft, Movement::Right) => Some((Movement::Bottom, true)),

        _ => None,
    }
}

type TileMap = Vec<Vec<Tile>>;

#[derive(Debug, Eq, PartialEq)]
enum Movement {
    Stop,
    Top,
    Bottom,
    Left,
    Right,
}

impl Movement {
    fn next_coords(&self, old_coords: (usize, usize)) -> (usize, usize) {
        match self {
            Movement::Top => (old_coords.0 - 1, old_coords.1),
            Movement::Bottom => (old_coords.0 + 1, old_coords.1),
            Movement::Left => (old_coords.0, old_coords.1 - 1),
            Movement::Right => (old_coords.0, old_coords.1 + 1),
            Movement::Stop => panic!("I said stop"),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
enum Tile {
    Start,
    Null,
    Horizontal,
    Vertical,
    TopRight,
    BottomRight,
    TopLeft,
    BottomLeft,
}

impl From<char> for Tile {
    fn from(value: char) -> Self {
        match value {
            'S' => Tile::Start,
            '.' => Tile::Null,
            '-' => Tile::Horizontal,
            '|' => Tile::Vertical,
            'L' => Tile::TopRight,
            'F' => Tile::BottomRight,
            'J' => Tile::TopLeft,
            '7' => Tile::BottomLeft,
            _ => panic!("Unmapped char in map: {}", value),
        }
    }
}

#[cfg(test)]
mod tests {
    use aoc_2023::challenge_data;

    use crate::{area, parse_tile_map, walk_loop, Tile, DAY};

    #[test]
    fn part1() {
        let data: Vec<String> = challenge_data(DAY).unwrap().map_while(Result::ok).collect();
        let (tile_map, start) = parse_tile_map(data);

        assert_eq!(Tile::Start, tile_map[start.0][start.1]);

        assert_eq!(Some(6640), walk_loop(&tile_map, start).map(|r| r.0 / 2));
    }

    #[test]
    fn part2() {
        let data: Vec<String> = challenge_data(DAY).unwrap().map_while(Result::ok).collect();
        let (tile_map, start) = parse_tile_map(data);

        assert_eq!(
            Some(411),
            walk_loop(&tile_map, start).map(|r| area(tile_map.len(), tile_map[0].len(), r.1))
        );
    }
}
