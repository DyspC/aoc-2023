use std::cmp::min;
use std::error::Error;

use itertools::Itertools;
use log::{debug, info, trace};
use rayon::prelude::*;

use aoc_2023::start;

const DAY: &str = module_path!();

/// In the giant field just outside, the springs are arranged into rows.
/// For each row, the condition records show every spring and whether it is operational (.) or damaged (#).
/// This is the part of the condition records that is itself damaged; for some springs, it is simply unknown (?) whether the spring is operational or damaged.
///
/// However, the engineer that produced the condition records also duplicated some of this information in a different format!
/// After the list of springs for a given row, the size of each contiguous group of damaged springs is listed in the order those groups appear in the row.
/// This list always accounts for every damaged spring, and each number is the entire size of its contiguous group (that is, groups are always separated by at least one operational spring: #### would always be 4, never 2,2).
///
/// So, condition records with no unknown spring conditions might look like this:
///
/// ```
/// #.#.### 1,1,3
/// .#...#....###. 1,1,3
/// .#.###.#.###### 1,3,1,6
/// ####.#...#... 4,1,1
/// #....######..#####. 1,6,5
/// .###.##....# 3,2,1
/// ```
///
/// However, the condition records are partially damaged; some of the springs' conditions are actually unknown (?).
///
/// For example:
///
/// ```
/// ???.### 1,1,3
/// .??..??...?##. 1,1,3
/// ?#?#?#?#?#?#?#? 1,3,1,6
/// ????.#...#... 4,1,1
/// ????.######..#####. 1,6,5
/// ?###???????? 3,2,1
/// ```
///
/// Equipped with this information, it is your job to figure out how many different arrangements of operational and broken springs fit the given criteria in each row.
///
/// In the first line (???.### 1,1,3), there is exactly one way separate groups of one, one, and three broken springs (in that order) can appear in that row: the first three unknown springs must be broken, then operational, then broken (#.#), making the whole row #.#.###.
///
/// The second line is more interesting: .??..??...?##. 1,1,3 could be a total of four different arrangements.
/// The last ? must always be broken (to satisfy the final contiguous group of three broken springs), and each ?? must hide exactly one of the two broken springs.
/// (Neither ?? could be both broken springs or they would form a single contiguous group of two; if that were true, the numbers afterward would have been 2,3 instead.)
/// Since each ?? can either be #. or .#, there are four possible arrangements of springs.
///
/// The last line is actually consistent with ten different arrangements!
/// Because the first number is 3, the first and second ? must both be . (if either were #, the first number would have to be 4 or higher).
/// However, the remaining run of unknown spring conditions have many different ways they could hold groups of two and one broken springs:
///
/// ```
/// ?###???????? 3,2,1
/// .###.##.#...
/// .###.##..#..
/// .###.##...#.
/// .###.##....#
/// .###..##.#..
/// .###..##..#.
/// .###..##...#
/// .###...##.#.
/// .###...##..#
/// .###....##.#
/// ```
///
/// In this example, the number of possible arrangements for each row is:
///
/// - ???.### 1,1,3 - 1 arrangement
/// - .??..??...?##. 1,1,3 - 4 arrangements
/// - ?#?#?#?#?#?#?#? 1,3,1,6 - 1 arrangement
/// - ????.#...#... 4,1,1 - 1 arrangement
/// - ????.######..#####. 1,6,5 - 4 arrangements
/// - ?###???????? 3,2,1 - 10 arrangements
///
/// Adding all of the possible arrangement counts together produces a total of 21 arrangements.
///
/// For each row, count all of the different arrangements of operational and broken springs that meet the given criteria.
/// What is the sum of those counts?
///
/// @see https://adventofcode.com/2023/day/12
fn main() -> Result<(), Box<dyn Error>> {
    let data: Vec<String> = start(DAY)?.map_while(Result::ok).collect();

    let problems = read_data(data, Part::P2(5));

    let res: usize = count_valid(problems);

    println!("Sum of all possibilities is {}", res);

    Ok(())
}

fn count_valid(problems: Vec<(Vec<SpringStatus>, Vec<usize>)>) -> usize {
    problems
        .par_iter()
        .enumerate()
        .map(|(idx, (springs, groups))| {
            debug!("[Line {}] Should make groups of {:?} from {:?}", idx, groups, springs);
            let c = generate_placements(springs, groups);
            info!("[Line {}] Found {}", idx, c);
            c
        })
        .sum()
}

/// Read the input and return all the galaxy coordinates
fn read_data(data: Vec<String>, part: Part) -> Vec<(Vec<SpringStatus>, Vec<usize>)> {
    data.iter()
        .map(move |line| {
            let v = line.splitn(2, ' ').collect_vec();
            match part {
                Part::P1 => (
                    v[0].chars().map(SpringStatus::from).collect_vec(),
                    v[1].split(',').map_while(|n| n.parse().ok()).collect_vec(),
                ),
                Part::P2(fold_level) => (
                    vec![v[0]]
                        .repeat(fold_level)
                        .join("?")
                        .chars()
                        .map(SpringStatus::from)
                        .collect_vec(),
                    v[1].split(',')
                        .map_while(|n| n.parse().ok())
                        .collect_vec()
                        .repeat(fold_level),
                ),
            }
        })
        .collect::<Vec<_>>()
}

enum Part {
    P1,
    P2(usize),
}

#[derive(Debug, Eq, PartialEq)]
enum SpringStatus {
    Ok,
    Broken,
    Unknown,
}

impl From<char> for SpringStatus {
    fn from(value: char) -> Self {
        match value {
            '?' => SpringStatus::Unknown,
            '.' => SpringStatus::Broken,
            '#' => SpringStatus::Ok,
            _ => unreachable!(),
        }
    }
}

struct Candidate<'a> {
    size: usize,
    ok_indices: &'a Vec<usize>,
    mask: Vec<&'a usize>,
    criteria: &'a Vec<usize>,
}

impl Candidate<'_> {
    fn valid(&self) -> bool {
        if self.ok_indices.len() + self.mask.len() != self.criteria.iter().sum() {
            return false;
        }
        let mut idx: usize = 0;
        let mut old = self.size;
        let values = self
            .mask
            .clone()
            .into_iter()
            .chain(self.ok_indices.iter())
            .sorted()
            .collect_vec();
        for &block_size in self.criteria {
            let chunk = &values[idx..idx + block_size];
            let start = chunk[0];
            if *start == old + 1 {
                return false;
            }
            let end = chunk[block_size - 1];
            if end - start != block_size - 1 {
                return false;
            }
            idx += block_size;
            old = *end;
        }
        true
    }
}

fn generate_placements(known: &Vec<SpringStatus>, lengths: &Vec<usize>) -> usize {
    // How tight it is to pack the intervals with 1 empty space in between
    let lease: isize = known.len() as isize - lengths.iter().sum::<usize>() as isize - lengths.len() as isize + 1;
    
    if lease < 0 {
        panic!("unresolvable");
    }
    
    generate_placements_rec(known.as_slice(), lengths.as_slice(), lease as usize + 1, String::new())
}

fn generate_placements_rec(known: &[SpringStatus], lengths: &[usize], possibilities: usize, printable: String) -> usize {
    if lengths.is_empty() { 
        return if known.iter().any(|e| e == &SpringStatus::Ok) {
            0
        } else {
            trace!("Should be valid {}-", printable);
            1
        };
    }
    
    if known.iter().filter(|&e| e == &SpringStatus::Ok).count() > lengths.iter().sum() {
        return 0;
    }
    
    (0..possibilities).into_par_iter()
        .map(|i| {let top = i + lengths[0];
        if known.len() < top {
            panic!("tout fucké");
        }
        if known[i..top].iter().any(|e| e == &SpringStatus::Broken) {
            return 0;
        }
        if (known.len() > top && known[top] == SpringStatus::Ok) || (i > 0 && known[..i].iter().any(|e| e == &SpringStatus::Ok)) {
            return 0;
        }

        let printable: String = printable.chars()
            .chain(".".repeat(i).chars())
            .chain("#".repeat(top - i).chars())
            .chain(".".chars())
            .collect();
            
        return generate_placements_rec(
            &known[min(known.len(), top+1)..],
            &lengths[1..],
            possibilities - i,
            printable
        );
    }).sum()
    // let mut res = 0;
    // for i in 0..possibilities {
    //     let top = i + lengths[0];
    //     if known.len() < top {
    //         panic!("tout fucké");
    //     }
    //     if known[i..top].iter().any(|e| e == &SpringStatus::Broken) {
    //         continue;
    //     }
    //     if (known.len() > top && known[top] == SpringStatus::Ok) || (i > 0 && known[..i].iter().any(|e| e == &SpringStatus::Ok)) {
    //         continue;
    //     }
    // 
    //     let printable: String = printable.chars()
    //         .chain(".".repeat(i).chars())
    //         .chain("#".repeat(top - i).chars())
    //         .chain(".".chars())
    //         .collect();
    //     res += generate_placements_rec(
    //         &known[min(known.len(), top+1)..],
    //         &lengths[1..],
    //         possibilities - i,
    //         printable
    //     );
    // }
    // res
}

#[cfg(test)]
mod tests {
    use aoc_2023::challenge_data;

    use crate::{count_valid, read_data, Part, DAY};

    #[test]
    fn part1() {
        let data: Vec<String> = challenge_data(DAY).unwrap().map_while(Result::ok).collect();

        let problems = read_data(data, Part::P1);
        let res: usize = count_valid(problems);
        assert_eq!(7379, res);
    }
}
